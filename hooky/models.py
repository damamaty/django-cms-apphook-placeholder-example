from django.db import models
from cms.models.fields import PlaceholderField


class Pet(models.Model):
    name = models.CharField(max_length=30)
    placeholder = PlaceholderField("pet_placeholder")
