from cms.app_base import CMSApp
from cms.apphook_pool import apphook_pool
from django.utils.translation import ugettext_lazy as _
from django.conf.urls import url, include

from .views import pets

@apphook_pool.register
class MyApphook(CMSApp):
    name = _("My Apphook")

    def get_urls(self, page=None, language=None, **kwargs):
        return [
            url(r'^pets/(?P<pet_name>[\w-]+)/$', pets, name="pets")
        ]