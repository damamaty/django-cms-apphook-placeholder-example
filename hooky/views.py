from django.shortcuts import render
from django.http import HttpResponseRedirect

from .models import Pet


def pets(request, pet_name):
    if request.method == "POST":
        pet = Pet.objects.create(name=pet_name)
        pet.save()

        return HttpResponseRedirect(request.path_info)

    elif request.method == "GET":
        pet = Pet.objects.filter(name=pet_name).first()

        return render(request, "pet.html", {"pet_name": pet_name, "pet": pet})
    elif request.method == "DELETE":
        raise Exception("Not implemented yet")
    else:
        raise Exception

