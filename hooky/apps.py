from django.apps import AppConfig


class HookyConfig(AppConfig):
    name = 'hooky'
